package com.zuitt.example;

import java.util.ArrayList;

public class Phonebook {

    private ArrayList<Contact> contacts = new ArrayList<Contact>();


    //default constructor
//    public Phonebook(){};

    //parameterized constructor
    public Phonebook(){
        contacts = new ArrayList<Contact>();
    }

    public ArrayList<Contact> getContact() {
        return this.contacts;
    }

    public void setContact(Contact contact) {
        contacts.add(contact);
    }
}
