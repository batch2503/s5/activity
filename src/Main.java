import com.zuitt.example.Contact;
import com.zuitt.example.Phonebook;

public class Main {
    public static void main(String[] args) {
        //System.out.println("Hello world!");

        Phonebook phonebook = new Phonebook();

        Contact contact1 = new Contact("John Doe", "+639152468596", "My home is in Quezon City");
        Contact contact2 = new Contact("Jane Doe", "+639162148573", "My home is in Caloocan City");

        phonebook.setContact(contact1);
        phonebook.setContact(contact2);

        if(phonebook.getContact().isEmpty()){
            System.out.println("Phonebook is empty.");
        }else{
            for(Contact contact: phonebook.getContact()){
                printInfo(contact);
            }
        }

    }

    public static void printInfo(Contact contact){
        System.out.println(contact.getName());
        System.out.println("------------------------------");

        if(contact.getContactNumber() != null){
            System.out.println(contact.getName() + " has the following registered number: " + contact.getContactNumber());
        }
        else {
            System.out.println(contact.getName() + "has no registered number");
        }

        if(contact.getAddress() != null){
            System.out.println(contact.getName() + " has the following registered address: " + contact.getAddress());
        }
        else{
            System.out.println(contact.getName() + "has no registered address");
        }
    }
}